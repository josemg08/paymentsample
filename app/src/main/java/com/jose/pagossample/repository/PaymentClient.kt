package com.jose.pagossample.repository

import com.jose.pagossample.model.BankModel
import com.jose.pagossample.model.FeesModel
import com.jose.pagossample.model.PaymentMethodModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PaymentClient {

    @GET("payment_methods$PUBLIC_KEY_APPEND")
    fun getListOfPaymentMethods(): Call<List<PaymentMethodModel>>

    @GET("payment_methods/card_issuers$PUBLIC_KEY_APPEND")
    fun getListOfBanks(@Query("payment_method_id") paymentMethodId: String): Call<List<BankModel>>

    @GET("payment_methods/installments$PUBLIC_KEY_APPEND")
    fun getListOfFees(@Query("amount") amount: String,
                      @Query("payment_method_id") paymentMethod: String,
                      @Query("issuer") issuer: String): Call<List<FeesModel>>

    companion object {
        private const val PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88"
        const val PUBLIC_KEY_APPEND = "?public_key=$PUBLIC_KEY"
    }

}