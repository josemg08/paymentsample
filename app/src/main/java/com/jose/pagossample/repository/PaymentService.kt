package com.jose.pagossample.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jose.pagossample.model.BankModel
import com.jose.pagossample.model.FeesModel
import com.jose.pagossample.model.PaymentMethodModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PaymentService {

    private val paymentMethodList = MutableLiveData<List<PaymentMethodModel>>()
    private val bankList = MutableLiveData<List<BankModel>>()
    private val feesOptionsList = MutableLiveData<List<FeesModel>>()

    fun fetchPaymentMethods() {
        val retrofit = Retrofit.Builder()
            .baseUrl(SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(PaymentClient::class.java)
        val jsonCall = service.getListOfPaymentMethods()
        jsonCall.enqueue(object : Callback<List<PaymentMethodModel>> {
            override fun onResponse(call: Call<List<PaymentMethodModel>>, response: Response<List<PaymentMethodModel>>) {
                paymentMethodList.value = (response.body() as ArrayList<PaymentMethodModel>?)!!
            }

            override fun onFailure(call: Call<List<PaymentMethodModel>>, t: Throwable) {
                //TODO
                Log.e("", "")
            }
        })
    }

    fun fetchBanks(paymentMethodId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(PaymentClient::class.java)
        val jsonCall = service.getListOfBanks(paymentMethodId)
        jsonCall.enqueue(object : Callback<List<BankModel>> {
            override fun onResponse(call: Call<List<BankModel>>, response: Response<List<BankModel>>) {
                bankList.value = (response.body() as ArrayList<BankModel>?)!!
            }

            override fun onFailure(call: Call<List<BankModel>>, t: Throwable) {
                //TODO
                Log.e("", "")
            }
        })
    }

    fun fetchFeesOptions(amount: String, paymentMethodId: String, bankId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(SERVICE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(PaymentClient::class.java)
        val jsonCall = service.getListOfFees(amount, paymentMethodId, bankId)
        jsonCall.enqueue(object : Callback<List<FeesModel>> {
            override fun onResponse(call: Call<List<FeesModel>>, response: Response<List<FeesModel>>) {
                feesOptionsList.value = (response.body() as ArrayList<FeesModel>?)!!
            }

            override fun onFailure(call: Call<List<FeesModel>>, t: Throwable) {
                //TODO
                Log.e("", "")
            }
        })
    }

    fun getBanks() = bankList

    fun getPaymentMethods() = paymentMethodList

    fun getFeesOptions() = feesOptionsList

    companion object {
        const val SERVICE_URL = "https://api.mercadopago.com/v1/"
    }

}