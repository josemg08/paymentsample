package com.jose.pagossample.repository

import androidx.lifecycle.LiveData
import com.jose.pagossample.model.BankModel
import com.jose.pagossample.model.FeesModel
import com.jose.pagossample.model.PaymentMethodModel

class PaymentRepository {

    private val paymentService = PaymentService()

    fun getPaymentMethods(): LiveData<List<PaymentMethodModel>> {
        paymentService.fetchPaymentMethods()
        return paymentService.getPaymentMethods()
    }

    fun getBanks(paymentMethodId: String): LiveData<List<BankModel>> {
        paymentService.fetchBanks(paymentMethodId)
        return paymentService.getBanks()
    }

    fun getFees(amount: String, paymentMethodId: String, bankId: String): LiveData<List<FeesModel>> {
        paymentService.fetchFeesOptions(amount, paymentMethodId, bankId)
        return paymentService.getFeesOptions()
    }

}