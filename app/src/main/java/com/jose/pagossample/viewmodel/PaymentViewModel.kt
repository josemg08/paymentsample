package com.jose.pagossample.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.jose.pagossample.model.BankModel
import com.jose.pagossample.model.FeesModel
import com.jose.pagossample.model.PaymentMethodModel
import com.jose.pagossample.repository.PaymentRepository

class PaymentViewModel: ViewModel() {

    private lateinit var paymentMethodId: String
    private lateinit var bankId: String
    private lateinit var feesRecommendedMessage: String

    private lateinit var banksList: LiveData<List<BankModel>>
    private lateinit var feesList: LiveData<List<FeesModel>>

    private val paymentRepository = PaymentRepository()
    private var paymentAmount: Int = 0

    private val paymentMethods = paymentRepository.getPaymentMethods()

    fun getPaymentMethods(): LiveData<List<PaymentMethodModel>> {
        paymentRepository.getPaymentMethods()
        return paymentMethods
    }

    fun setPaymentAmount(amount: Int) {
        paymentAmount = amount
    }

    fun getPaymentMethodAt(position: Int): PaymentMethodModel? {
        return paymentMethods.value?.get(position)
    }

    fun getAmount(): Int {
        return paymentAmount
    }

    fun getListOfBanks(): LiveData<List<BankModel>> {
        banksList = paymentRepository.getBanks(paymentMethodId)
        return banksList
    }

    fun getListOfFees(): LiveData<List<FeesModel>> {
        feesList = paymentRepository.getFees(paymentAmount.toString(), paymentMethodId, bankId)
        return feesList
    }

    fun setPaymentMethodId(paymentMethodId: String) {
        this.paymentMethodId = paymentMethodId
    }

    fun getPaymentMethodId(): String {
        return paymentMethodId
    }

    fun setPaymentMethodPositionSelected(position: Int) {
        paymentMethodId = paymentMethods.value!![position].id
    }

    fun setBankPositionSelected(position: Int) {
        bankId = banksList.value!![position].id
    }

    fun setFeesPositionSelected(position: Int) {
        feesRecommendedMessage = feesList.value!![0].payer_costs[position].recommended_message
    }

    fun getFeesRecommendedMessage(): String {
        return feesRecommendedMessage
    }

    fun getBankId(): String {
        return bankId
    }

}