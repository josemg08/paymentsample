package com.jose.pagossample.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jose.pagossample.R
import com.jose.pagossample.model.BankModel

class BanksItemAdapter(private val list: List<BankModel>,
                       private val clickListener: View.OnClickListener)
    : RecyclerView.Adapter<BanksItemAdapter.ViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.banks_item_adapter, viewGroup, false)
        view.setOnClickListener(clickListener)
        context = viewGroup.context
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.title.text = list[position].name

        Glide
            .with(context)
            .load(list[position].thumbnail)
            .into(viewHolder.photo)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.title)!!
        val photo = itemView.findViewById<ImageView>(R.id.photo)!!
    }

}