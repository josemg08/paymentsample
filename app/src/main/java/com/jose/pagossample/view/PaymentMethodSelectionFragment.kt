package com.jose.pagossample.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.jose.pagossample.model.PaymentMethodModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jose.pagossample.R
import com.jose.pagossample.viewmodel.PaymentViewModel

class PaymentMethodSelectionFragment : Fragment(), View.OnClickListener{

    private lateinit var cantAccessInfoText: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment_method_selection, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        cantAccessInfoText = view!!.findViewById(R.id.cantAccessInfo)
        progressBar = view!!.findViewById(R.id.progressBar)
        recyclerView = view!!.findViewById(R.id.paymentMethodRecyclerView)

        showProgress()

        var mediaAdapter: RecyclerView.Adapter<*>

        viewModel = activity?.run {
            ViewModelProviders.of(this).get(PaymentViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        viewModel.getPaymentMethods().observe(this, Observer<List<PaymentMethodModel>> {
            if (it.isEmpty()) {
                showErrorMessage()
            } else {
                showContent()
                val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                recyclerView.layoutManager = layoutManager
                mediaAdapter = PaymentMethodItemAdapter(it!!, this)
                recyclerView.adapter = mediaAdapter
            }
        })
    }

    override fun onResume() {
        super.onResume()
        showProgress()
    }

    override fun onClick(view: View?) {
        val itemPosition = view?.let { recyclerView.getChildLayoutPosition(it) }
        viewModel.setPaymentMethodPositionSelected(itemPosition!!)
        navigateToBankSelection()
    }

    private fun navigateToBankSelection() {
        val navigateToNext = PaymentMethodSelectionFragmentDirections
            .actionGoToBankSelection()
        Navigation.findNavController(this.view!!).navigate(navigateToNext)
    }

    //TODO we could have this in an abstract fragment and have the others extend that
    private fun showProgress() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.INVISIBLE
        cantAccessInfoText.visibility = View.INVISIBLE
    }

    private fun showContent() {
        progressBar.visibility = View.INVISIBLE
        recyclerView.visibility = View.VISIBLE
        cantAccessInfoText.visibility = View.INVISIBLE
    }

    private fun showErrorMessage() {
        progressBar.visibility = View.INVISIBLE
        recyclerView.visibility = View.INVISIBLE
        cantAccessInfoText.visibility = View.VISIBLE
    }

}