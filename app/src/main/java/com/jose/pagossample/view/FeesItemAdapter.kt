package com.jose.pagossample.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.jose.pagossample.R
import com.jose.pagossample.model.FeesInstallmentsModel

class FeesItemAdapter(private val list: List<FeesInstallmentsModel>,
                      private val clickListener: View.OnClickListener)
    : RecyclerView.Adapter<FeesItemAdapter.ViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.fees_item_adapter, viewGroup, false)
        view.setOnClickListener(clickListener)
        context = viewGroup.context
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.title.text = list[position].recommended_message
        viewHolder.instalments.text = list[position].installment_amount.toString()
        viewHolder.finalAmount.text = list[position].total_amount.toString()
        viewHolder.maximum.text = list[position].max_allowed_amount.toString()
        viewHolder.minimum.text = list[position].min_allowed_amount.toString()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.title)!!
        val minimum = itemView.findViewById<TextView>(R.id.minimum)!!
        val maximum = itemView.findViewById<TextView>(R.id.maximum)!!
        val instalments = itemView.findViewById<TextView>(R.id.instalments)!!
        val finalAmount = itemView.findViewById<TextView>(R.id.finalAmount)!!
    }

}