package com.jose.pagossample.view

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.jose.pagossample.R
import com.jose.pagossample.viewmodel.PaymentViewModel
import kotlinx.android.synthetic.main.fragment_amount_input.*

class AmountInputFragment : Fragment() {

    private lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_amount_input, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val amountText = view.findViewById<EditText>(R.id.amount)

        viewModel = activity?.run {
            ViewModelProviders.of(this).get(PaymentViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        arguments?.let {
            val safeArgs = AmountInputFragmentArgs.fromBundle(it)
            if (safeArgs.sequenceFinished) {
                showFinishDialog()
            }
        }

        goToNextButton.setOnClickListener {
            viewModel.setPaymentAmount(amountText.text.toString().toInt())
            val navigateToNext = AmountInputFragmentDirections
                .actionGoToPaymentMethodSelection()
            Navigation.findNavController(it).navigate(navigateToNext)
        }
    }

    private fun showFinishDialog() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(viewModel.getFeesRecommendedMessage())
        builder.setMessage("Amount = "+viewModel.getAmount().toString()
                +" Payment method Id = "+viewModel.getPaymentMethodId()
                +" Bank Id = "+viewModel.getBankId())
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}