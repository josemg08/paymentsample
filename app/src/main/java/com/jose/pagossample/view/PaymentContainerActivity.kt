package com.jose.pagossample.view

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle

import com.jose.pagossample.R
import kotlinx.android.synthetic.main.activity_payment_container.*

class PaymentContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_container)

        setSupportActionBar(toolbar)
    }

}
