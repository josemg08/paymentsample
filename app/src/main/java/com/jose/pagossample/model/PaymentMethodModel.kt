package com.jose.pagossample.model

data class PaymentMethodModel(
    val id: String,
    val name: String,
    val payment_type_id: String,
    val status: String,
    val secure_thumbnail: String,
    val thumbnail: String,
    val deferred_capture: String,

    val min_allowed_amount: Double,
    val max_allowed_amount: Double,
    val accreditation_time: Double
    //TODO add settings, additional_info_needed, financial_institutions and processing_modes
)