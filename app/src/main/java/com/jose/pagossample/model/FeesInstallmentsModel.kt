package com.jose.pagossample.model

data class FeesInstallmentsModel(
    val installments: Int,
    val installment_rate: Double,
    val discount_rate: Double,
    val min_allowed_amount: Double,
    val max_allowed_amount: Double,
    val recommended_message: String,
    val installment_amount: Double,
    val total_amount: Double
)
//TODO add labels, installment_rate_collector