package com.jose.pagossample.model

data class BankModel(
    val id: String,
    val name: String,
    val secure_thumbnail: String,
    val thumbnail: String,
    val processing_mode: String,
    val merchant_account_id: String
)