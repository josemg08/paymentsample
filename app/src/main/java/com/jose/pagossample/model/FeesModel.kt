package com.jose.pagossample.model

data class FeesModel(
    val payment_method_id: String,
    val payment_type_id: String,
    val issuer: BankModel,
    val processing_mode: String,
    val merchant_account_id: String,
    val payer_costs: List<FeesInstallmentsModel>
)